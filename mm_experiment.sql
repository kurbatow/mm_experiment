-- SQL Manager Lite for PostgreSQL 5.9.1.49393
-- ---------------------------------------
-- Хост         : localhost
-- База данных  : mm_experiment
-- Версия       : PostgreSQL 10.1 on x86_64-pc-mingw64, compiled by gcc.exe (Rev5, Built by MSYS2 project) 4.9.2, 64-bit



SET check_function_bodies = false;
--
-- Structure for table user (OID = 16387) : 
--
SET search_path = public, pg_catalog;
CREATE TABLE public."user" (
    id serial NOT NULL,
    username varchar,
    name varchar,
    email varchar
)
WITH (oids = false);
--
-- Structure for table entity (OID = 16398) : 
--
CREATE TABLE public.entity (
    id serial NOT NULL,
    name varchar,
    description varchar,
    other varchar
)
WITH (oids = false);
--
-- Structure for table relation_type (OID = 16409) : 
--
CREATE TABLE public.relation_type (
    id integer DEFAULT nextval('relations_type_id_seq'::regclass) NOT NULL,
    name varchar
)
WITH (oids = false);
--
-- Structure for table relation (OID = 16420) : 
--
CREATE TABLE public.relation (
    id serial NOT NULL,
    entity_1_id integer,
    entity_2_id integer,
    relation_type_id integer
)
WITH (oids = false);
--
-- Data for table public."user" (OID = 16387) (LIMIT 0,4)
--
INSERT INTO "user" (id, username, name, email)
VALUES (1, 'admin', 'Администратор', 'admin@test.com');

INSERT INTO "user" (id, username, name, email)
VALUES (2, 'test', 'Тест', 'test@test.com');

INSERT INTO "user" (id, username, name, email)
VALUES (3, 'ivanov', 'Иванов', 'ivanov@test.com');

INSERT INTO "user" (id, username, name, email)
VALUES (4, 'andreev', 'Андреев', 'andreev@test.com');

--
-- Data for table public.entity (OID = 16398) (LIMIT 0,3)
--
INSERT INTO entity (id, name, description, other)
VALUES (1, 'Сущность 1', 'Первая', '11');

INSERT INTO entity (id, name, description, other)
VALUES (2, 'Сущность 2', 'Вторая', '22');

INSERT INTO entity (id, name, description, other)
VALUES (3, 'Сущность 3', 'Третья', '33');

--
-- Data for table public.relation_type (OID = 16409) (LIMIT 0,3)
--
INSERT INTO relation_type (id, name)
VALUES (1, 'Тип 1');

INSERT INTO relation_type (id, name)
VALUES (3, 'Тип 2');

INSERT INTO relation_type (id, name)
VALUES (4, 'Тип 3');

--
-- Data for table public.relation (OID = 16420) (LIMIT 0,5)
--
INSERT INTO relation (id, entity_1_id, entity_2_id, relation_type_id)
VALUES (1, 1, 1, 1);

INSERT INTO relation (id, entity_1_id, entity_2_id, relation_type_id)
VALUES (2, 1, 2, 3);

INSERT INTO relation (id, entity_1_id, entity_2_id, relation_type_id)
VALUES (3, 2, 3, 1);

INSERT INTO relation (id, entity_1_id, entity_2_id, relation_type_id)
VALUES (4, 3, 3, 1);

INSERT INTO relation (id, entity_1_id, entity_2_id, relation_type_id)
VALUES (5, 2, 2, 1);

--
-- Definition for index user_pkey (OID = 16394) : 
--
ALTER TABLE ONLY "user"
    ADD CONSTRAINT user_pkey
    PRIMARY KEY (id);
--
-- Definition for index entity_pkey (OID = 16405) : 
--
ALTER TABLE ONLY entity
    ADD CONSTRAINT entity_pkey
    PRIMARY KEY (id);
--
-- Definition for index relations_type_pkey (OID = 16416) : 
--
ALTER TABLE ONLY relation_type
    ADD CONSTRAINT relations_type_pkey
    PRIMARY KEY (id);
--
-- Definition for index relation_pkey (OID = 16424) : 
--
ALTER TABLE ONLY relation
    ADD CONSTRAINT relation_pkey
    PRIMARY KEY (id);
--
-- Definition for index relation_fk (OID = 16426) : 
--
ALTER TABLE ONLY relation
    ADD CONSTRAINT relation_fk
    FOREIGN KEY (entity_1_id) REFERENCES entity(id);
--
-- Definition for index relation_fk1 (OID = 16431) : 
--
ALTER TABLE ONLY relation
    ADD CONSTRAINT relation_fk1
    FOREIGN KEY (entity_2_id) REFERENCES entity(id);
--
-- Definition for index relation_fk2 (OID = 16436) : 
--
ALTER TABLE ONLY relation
    ADD CONSTRAINT relation_fk2
    FOREIGN KEY (relation_type_id) REFERENCES relation_type(id);
--
-- Data for sequence public.user_id_seq (OID = 16385)
--
SELECT pg_catalog.setval('user_id_seq', 4, true);
--
-- Data for sequence public.entity_id_seq (OID = 16396)
--
SELECT pg_catalog.setval('entity_id_seq', 3, true);
--
-- Data for sequence public.relations_type_id_seq (OID = 16407)
--
SELECT pg_catalog.setval('relations_type_id_seq', 4, true);
--
-- Data for sequence public.relation_id_seq (OID = 16418)
--
SELECT pg_catalog.setval('relation_id_seq', 5, true);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';
